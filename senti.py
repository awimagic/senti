# s = 'innovative medicines'

# data1 = data[data['mod'].str.contains('innovative medicines', flags=re.I)]


# for text in data1['orig']:
#     sents = [sent for sent in nltk.sent_tokenize(text) if re.search('innovative medicines', sent, flags=re.I)]
#     print(sents)
#     break


# data2 = data[data['orig'].apply(lambda text: all((re.search(ss, text, flags=re.I) for ss in ['new innovative medicines', 'reimbursement list'])))]


# for text in data2['orig']:
#     sents = [sent for sent in nltk.sent_tokenize(text) if all((re.search(ss, sent, flags=re.I) for ss in  ['new innovative medicines', 'reimbursement list']))]



#     # find all sentences that have 'reimbursement list'


# def

import re
from nltk import sent_tokenize

data1 = data[data['mod'].str.contains(r' reimbursement list ', flags=re.I)]

sents = data1['mod'].apply(lambda s: [sent for sent in sent_tokenize(s) if re.search(r' reimbursement list ', sent, flags=re.I)])

sents1 = [s1 for s2 in sents for s1 in s2]


from nltk.parse.stanford import StanfordDependencyParser
from nltk.parse.stanford import StanfordParser

import pprint

path_to_jar = ('/home/m/stanfordnlp/for_nltk/stanford-parser-full-2015-12-09/'
               'stanford-parser.jar')

path_to_models_jar = ('/home/m/stanfordnlp/for_nltk/stanford-parser-full-2015-12-09/'
                      'stanford-parser-3.6.0-models.jar')

parser = StanfordParser(path_to_jar, path_to_models_jar)
dparser = StanfordDependencyParser(path_to_jar, path_to_models_jar)

# x = parser.raw_parse(sents.iloc[0][0])
# y = next(x)
# print(type(y))
# print(y)

# find a subtree that ha

import appyutils as au

def xxx(s):
    au.hl('reimbursement list', s)
    next(parser.raw_parse(s)).draw()
