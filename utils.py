import pandas as pd
import re
import os
from colorama import Fore


def dump(series, n):
    if not os.path.exists(n):
        os.mkdir(n)
    for i, e in series.iteritems():
        with open('{}/{}.txt'.format(n, i), 'w') as f:
            f.write(str(e))
    print('wrote', n)


def country(s):
    lines = s.split('\n')
    if re.match(r'Published: \d{1,2} [A-Z][a-z]{2} \d{4}'
                '|' r'Data Release Date: \d{1,2} [A-Z][a-z]{2} \d{4}', s):
        if len(lines) >= 3:
            return lines[2].split(':')[0].strip()
        else:
            print(Fore.RED, 'ERROR: No Country Found', Fore.RESET)
            print(s)
            return None
    elif re.match(r'FULL REPORT: Print \| Word \| PDF', s):
        return re.sub('Country Summary - '
                      '|' r' ?THIS SECTION: Print \| Word \| PDF',
                      '', lines[1].strip())
    else:
        print(Fore.RED, 'ERROR: No Country Found, for the following story', Fore.RESET)
        print('    ', s)
        return None


def date(s):
    x = re.search(r'(\d{1,2}) (jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec) (\d{4})', s, flags=re.I)
    if x:
        return x.group(0)
    else:
        print(Fore.RED, 'ERROR - No Date Found, for the following story', Fore.RESET)
        print('    ', s)
        return None


def prepare_data(files=[]):
    if not files:
        return None

    dfs = []
    for n, f in enumerate(files, start=1):
        print('Reading file:', n, f)
        d = pd.read_excel(f, verbose=True, header=None,
                          parse_cols=[1], names=['orig'])
        d['file_num'] = n
        d['cell_num'] = d.index + 1
        dfs.append(d)

    df = pd.concat(dfs, ignore_index=True)

    print('Num rows', df.shape[0])
    df.fillna('', inplace=True)
    df['orig'] = df['orig'].apply(str.strip)
    print('dropping duplicates...')
    df.drop_duplicates(subset=['orig'], inplace=True)
    print('Num rows after dropping duplicates', df.shape[0])

    regex = r'Published: \d{2} [A-Z][a-z]{2} \d{4}' \
            '|' r'Analyst Contact Details:' \
            '|' r'FULL REPORT: Print \| Word \| PDF' \
            '|' r'Life Sciences - Analysis Print \| Word \| PDF'

    data = df[df['orig'].str.contains(regex, regex=True)].copy()
    data['country'] = data['orig'].apply(country)
    data['date'] = data['orig'].apply(date)
    print(Fore.RED, 'dropping stories with no country tag or date tag',
          Fore.RESET)
    data.dropna(subset=['country', 'date'], inplace=True)

    data['date'] = pd.to_datetime(data['date'])

    # remove fixtures and artifacts
    regex = r'Published: \d{2} [A-Z][a-z]{2} \d{4}' \
            '|' r'\n?Analyst Contact Details:.+?$' \
            '|' r'FULL REPORT: Print \| Word \| PDF' \
            '|' r'THIS SECTION: Print \| Word \| PDF' \
            '|' r'This information was last updated on.+?Email alerts on subsections only' \
            '|' r'E-mail Alert: Yes No' \
            '|' r'Life Sciences - Analysis Print \| Word \| PDF' \
            '|' r'Data Release Date: \d{2} [A-Z][a-z]{2} \d{4}' \
            '|' r'\(see .*?\)'
    #            '|' r'\d+' \
            #         '|' r'Significance: ' \
            # '|' r'IHS Global Insight Perspective' \
            # '|' r'year-on-year' \

    data['mod'] = data['orig'].apply(lambda s: re.sub(regex, '', s).strip())

    # put a full stop at the end of topic/abstract line at the
    # beginning of the story
    regex = r'(?<!\.)\r\n(?=\w)'  # a '\r\n' that is not preceded by a
                                  # period and followed by a printable
    data['mod'] = data['mod'].apply(lambda s: re.sub(regex, r'.\r\n', s).strip())

    data.index = range(data.shape[0])
    return data
