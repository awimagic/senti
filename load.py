from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import time
import utils
import numpy as np
import pandas as pd


data_sources = ['./data/Argentina, Australia, Belgium. Brazil and Canada files till 2010.xlsx',
                './data/Canada, Chile, China, Czech Republic, Denmark Files 2010 to 2016.xlsx',
                './data/First 20 Countries_All Stories_2005to2016.xlsx',
                './data/France, Germany, Greece and Hungary 2010 to 2016.xlsx',
                './data/India 2010 to 2016.xlsx',
                './data/Ireland, Italy, Japan, Lebanon, and Mexico 2010 to 2016.xlsx',
                './data/Netherlands, New Zealand, Poland, Portugal, Russia, South Africa, South Korea and Spain.xlsx',
                './data/Next 20 Countries_All Stories_2005to2016.xlsx',
                './data/Sweden, Switzerland, Thailand, Turkey, UK and Venezuela.xlsx',
                './data/United States_Stories from Oct 2012 to Jan 2010.xlsx',
                './data/United States_Stories till June 2012.xlsx']

start = time.time()
data = utils.prepare_data(data_sources)
end = time.time()
print('data ingestion done. time taken:', end - start)

tv = TfidfVectorizer(stop_words='english', ngram_range=(1, 3))
start = time.time()
print('starting to build document-term matrix ...')
tfidf = tv.fit_transform(data['mod'])
end = time.time()
print('done, time taken', end - start)
