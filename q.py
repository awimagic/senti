import dateutil
import os
import re
import ipdb
import appyutils as au
import nltk
from textblob import TextBlob
import numpy as np


def sentiment(dataframe, query):
    scores = []
    for report in dataframe['mod']:
        sents = [sent for sent in nltk.sent_tokenize(report)
                 if re.search(query, sent, flags=re.I)]
        scores.append(
            sum(TextBlob(sent).sentiment.polarity for sent in sents)/len(sents) if sents else np.nan)
    dataframe['sent_score'] = scores


raw_q = 'price cuts in China'
while raw_q:
    raw_q = input('Enter query: (e.g. price cuts in china OR enter nothing to quit) ').strip()
    if not raw_q:
        print('bye')
        break
    q = tv.transform([raw_q])
    cs = cosine_similarity(q, tfidf).ravel()
    csi = list(zip(cs, range(len(cs))))

    senti_q = input('Enter sentiment analysis key: (e.g. price cuts OR leave blank) ').strip()

    # remove zero similarity results
    csi = [t for t in csi if t[0] > 0]
    nnz = len(csi)              # number of non-zero results
    print('Number of matching documents: ', nnz)
    if not nnz:
        continue

    country_filter = input('Leave blank OR Enter country filter (e.g. china): ').strip()
    if country_filter:
        rindices = [t[1] for t in csi]
        rdf = data.iloc[rindices]
        cindices =  rdf[rdf['country'].str.contains(country_filter, case=False)].index
        csi = [t for t in csi if t[1] in cindices]
        print('Number of matching documents after country filter(={}): '.format(country_filter), len(csi))

    oldest_date = data.iloc[[t[1] for t in csi]]['date'].min()
    newest_date = data.iloc[[t[1] for t in csi]]['date'].max()
    prompt = '''Leave blank OR Enter comma separated date range <start_date, end_date>
Oldest date available now: {}
Newest date available now: {}
 (e.g. 2012-10-01, 2015): '''.format(
        oldest_date.strftime('%Y-%m-%d'),
        newest_date.strftime('%Y-%m-%d'))
    date_filter = input(prompt)

    if date_filter:
        start, end = date_filter.split(',')
        try:
            sdate = dateutil.parser.parse(start, default=oldest_date)
        except:
            print('start date not valid ...')
            continue

        try:
            edate = dateutil.parser.parse(end, default=newest_date)
        except:
            print('end date not valid ...')
            continue

        if sdate > edate:
            print('start_date later than end_date')
            continue
            
        rindices = [t[1] for t in csi]
        rdf = data.iloc[rindices]
        rdf = rdf[(sdate <= rdf['date']) & (rdf['date'] <= edate)]
        csi = [t for t in csi if t[1] in rdf.index]
        print('Number of matching documents after date filter({}, {}): '.format(
            sdate.strftime('%Y-%m-%d'), edate.strftime('%Y-%m-%d')), len(csi))

    topn = int(input('Leave blank of Enter the number of results wanted(sorted by relevance)? (max: {}): '.format(len(csi))).strip())
    assert topn <= len(csi), 'Can return at max, {} results'.format(len(csi))

    if not topn:
        topn = len(csi)

    csi = sorted(csi, key=lambda x: x[0], reverse=True)[:topn]
    rindices = [t[1] for t in csi]
    rdf = data.iloc[rindices].copy()
    rdf['sim_score'] = [t[0] for t in csi]
    rdf['word_count_orig'] = rdf['orig'].apply(lambda s: len(s.split()))

    sentiment(rdf, senti_q)
    
    with pd.option_context('expand_frame_repr', False, 'max_colwidth', 50):
        rdf['preview'] = rdf['mod'].apply(lambda s: re.sub(r'.*?:', '', s).strip())
        print(rdf[['sim_score', 'sent_score', 'country', 'date', 'word_count_orig', 'preview']])

    ofile = input('Leave blank or Enter CSV filename to output results (e.g. out.csv) ').strip()
    if ofile:
        rdf[['sim_score', 'sent_score', 'country', 'date', 'word_count_orig', 'orig']].to_csv(ofile)
        fpath = os.path.join(os.getcwd(), ofile)
        print('written results to file: {}'.format(fpath))
